class Micropost < ActiveRecord::Base
    belongs_to :user ## それぞれの投稿は特定１人のユーザーのものとして設定
    validates :user_id, presence: true #user_id が存在すること
    validates :content, presence: true, length: {maximum: 140} #contentがあり、140字以内
    mount_uploader :image, ImageUploader #carrierwaveで追加

end
