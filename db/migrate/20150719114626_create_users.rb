class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      
      t.timestamps
      
      t.index :email,  unique: true #追加した行
    end
  end
end

